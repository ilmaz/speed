!    Copyright (C) 2012 The SPEED FOUNDATION
!    Author: Ilario Mazzieri
!
!    This file is part of SPEED.
!
!    SPEED is free software; you can redistribute it and/or modify it
!    under the terms of the GNU Affero General Public License as
!    published by the Free Software Foundation, either version 3 of the
!    License, or (at your option) any later version.
!
!    SPEED is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Affero General Public License for more details.
!
!    You should have received a copy of the GNU Affero General Public License
!    along with SPEED.  If not, see <http://www.gnu.org/licenses/>.


!> @brief Makes not-honoring technique. Mechanical properties given node by node.



     subroutine MAKE_MECH_PROP_CASE_045(rho, lambda, mu, gamma, qs, qp, & !outputs
                                        xs, ys, zs, Depth, zs_all,&
                                        vs30, thickness, sub_tag_all)

     real*8, intent(out) :: rho, lambda, mu, gamma, qs, qp
     real*8, intent(in)  :: xs, ys, zs, Depth, zs_all,&
                            vs30, thickness             
     real*8              :: ni, VS, VP, Depth_real
         integer*4, intent(in) :: sub_tag_all   

     rho    = 0.d0;
     lambda = 0.d0;
     mu     = 0.d0;
     gamma  = 0.d0;
     qs     = 0.d0;
     qp     = 0.d0;


    !
    !-------------------------------------------------------------------
        ! + MATERIAL INSIDE THE CLAY BASIN - 1st Layer

    if (sub_tag_all.eq.1) then
                
            VS = 70.d0 + Depth *((100.d0-70.d0)/(zs_all+Depth+0.1d0));
            VP  = VS * 13.d0;
            rho = 1400.d0 + Depth *((1600.d0-1400.d0)/(zs_all+Depth0+0.1d0));
            lambda = rho * (VP**2.d0 - 2.d0*VS**2.d0);
            mu = rho * VS**2.d0;
            qs = 40.d0 + Depth *((55.d0-40.d0)/(zs_all+Depth+0.1d0));
                        qp = qs*2.d0;
            gamma = 4.d0*datan(1.d0)/qs;
                        
    ! + MATERIAL INSIDE THE DEEP BASIN - 2nd Layer
    elseif (sub_tag_all.eq.2) then
            VS = 500.d0 + 9.6d0 * Depth**(0.6d0);
            VP  = 1500.d0 + 19.2d0 * Depth**(0.6d0);
            rho = 1900.d0;
            lambda = rho * (VP**2.d0 - 2.d0*VS**2.d0);
            mu = rho * VS**2.d0;
            qs = VS/6.d0;
                        qp = qs*2.d0;
            gamma = 4.d0*datan(1.d0)/qs;
    ! + First 200 m of BEDROCK LAYER
        elseif (Depth .le. 200.0d0) then        
                        VS = 750.d0;
            VP  = 1700.d0;
            rho = 2000.d0;
            lambda = rho * (VP**2.d0 - 2.d0*VS**2.d0);
            mu = rho * VS**2.d0;
            qs = 100.d0;
                        qp = qs*2.d0;
            gamma = 4.d0*datan(1.d0)/qs;
    else ! (1st BEDROCK LAYER)
            VS = 1550.d0;
            VP  = 2782.d0;
            rho = 2200.d0;
            lambda = rho * (VP**2.d0 - 2.d0*VS**2.d0);
            mu = rho * VS**2.d0;
            qs = VS/10.d0;
                        qp = qs*2.d0;
            gamma = 4.d0*datan(1.d0)/qs;
    endif


     end subroutine MAKE_MECH_PROP_CASE_045
                                             
