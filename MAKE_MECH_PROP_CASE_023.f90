!    Copyright (C) 2012 The SPEED FOUNDATION
!    Author: Ilario Mazzieri
!
!    This file is part of SPEED.
!
!    SPEED is free software; you can redistribute it and/or modify it
!    under the terms of the GNU Affero General Public License as
!    published by the Free Software Foundation, either version 3 of the
!    License, or (at your option) any later version.
!
!    SPEED is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Affero General Public License for more details.
!
!    You should have received a copy of the GNU Affero General Public License
!    along with SPEED.  If not, see <http://www.gnu.org/licenses/>.


!> @brief Makes not-honoring technique. Mechanical properties given node by node.


     subroutine MAKE_MECH_PROP_CASE_023(rho, lambda, mu, gamma, qs, qp, & !outputs
                                        xs, ys, zs, Depth, zs_all,&
                                        vs30, thickness, sub_tag_all)
                      
real*8, intent(out)   :: rho, lambda, mu, gamma, qs, qp
real*8, intent(in)    :: xs, ys, zs, zs_all,&
                         vs30,thickness
integer*4             :: sub_tag_all		                            
real*8, intent(inout) :: Depth                       		
real*8                :: ni, VS, VP, Depth_real                                                 
                      
rho    = 0.d0;
lambda = 0.d0;
mu     = 0.d0;
gamma  = 0.d0;
qs     = 0.d0;
qp     = 0.d0;

if (Depth .lt. 0.0d0) Depth = 0.0d0

!if (Depth .lt. 1000.d0) then
!     VS = 1200.d0 + 46.51*(Depth)**(0.5)
!     VP  = VS*1.86;
!     VP  = VS*1.80;
!     rho = 2000.d0 + 18.87*(Depth)**(0.5)
!     qs = 0.08*VS;
!     qp = 0.09*VP;
if (Depth .lt. 2000.d0) then
     VS = 1200.d0 + 46.51*(Depth)**(0.5)
!     VP  = VS*1.74;
     VP  = VS*1.80;
     rho = 2000.d0 + 18.87*(Depth)**(0.5)
     qs = 0.13*VS;
     qp = 0.12*VP;
else
     VS = 3280.d0
     VP  = VS*1.74;
     rho = 2844.d0
     qs = 250;
     qp = 500;
endif

lambda = rho * (VP**2.d0 - 2.d0*VS**2.d0);
mu = rho * VS**2;
gamma = 4.d0*datan(1.d0)/qs;

                                                     
end subroutine MAKE_MECH_PROP_CASE_023                                 
